package ru.solovgeo.calculator

import com.nhaarman.mockito_kotlin.anyVararg
import com.nhaarman.mockito_kotlin.eq
import org.hamcrest.Matchers
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import ru.solovgeo.calculator.business.PRECISION
import ru.solovgeo.calculator.business.ParseInteractor
import ru.solovgeo.calculator.data.IResourceProvider
import java.math.BigDecimal
import java.math.MathContext

@RunWith(MockitoJUnitRunner::class)
class ParseInteractorTest {

    @Mock
    lateinit var resourceProvider: IResourceProvider
    lateinit var interactor: ParseInteractor

    @Before
    fun init() {
        `when`(resourceProvider.getString(eq(R.string.exception_expect_number), anyVararg())).thenReturn("0")
        `when`(resourceProvider.getString(eq(R.string.exception_two_dots), anyVararg())).thenReturn("1")
        `when`(resourceProvider.getString(eq(R.string.exception_wrong_symbol_in_number), anyVararg())).thenReturn("2")
        `when`(resourceProvider.getString(eq(R.string.exception_nothing_after_dot), anyVararg())).thenReturn("3")
        `when`(resourceProvider.getString(eq(R.string.exception_expect_right_bracket), anyVararg())).thenReturn("4")
        `when`(resourceProvider.getString(eq(R.string.exception_unexpected_symbol), anyVararg())).thenReturn("5")
        `when`(resourceProvider.getString(eq(R.string.exception_unexpected_right_bracket), anyVararg())).thenReturn("6")
        interactor = ParseInteractor(resourceProvider)
    }

    @Test
    fun testGetNumberSuccess() {
        interactor.parse("      504.3  37 54 5-5")
        assertThat(BigDecimal(504.3, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.getNumber()))
        assertThat(BigDecimal(37.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.getNumber()))
        assertThat(BigDecimal(54.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.getNumber()))
        assertThat(BigDecimal(5.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.getNumber()))

        interactor.parse("2     ")
        assertThat(BigDecimal(2.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.getNumber()))

        interactor.parse(".50")
        assertThat(BigDecimal(0.5, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.getNumber()))

        interactor.parse("1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")
        System.out.print(interactor.getNumber())
    }

    @Test
    fun testGetNumberDotsFail() {
        interactor.parse("50.4.3")
        try {
            interactor.getNumber()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("1", exception.message)
        }

        interactor.parse("50.")
        try {
            interactor.getNumber()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("3", exception.message)
        }

        interactor.parse("50.-0")
        try {
            interactor.getNumber()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("2", exception.message)
        }

        interactor.parse("50. 0")
        try {
            interactor.getNumber()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("3", exception.message)
        }
    }

    @Test
    fun testGetNumberNoNumberFail() {
        interactor.parse("")
        try {
            interactor.getNumber()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }

        interactor.parse("-")
        try {
            interactor.getNumber()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }
    }

    @Test
    fun testWhitespacesMultiplicationSuccess() {
        interactor.parse("2*3")

        assertThat(BigDecimal(6.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
        interactor.parse("-2*-3")
        assertThat(BigDecimal(6.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
        interactor.parse("2 * 3")
        assertThat(BigDecimal(6.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
        interactor.parse("2 * -3")
        assertThat(BigDecimal(-6.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
        interactor.parse("    -2 * -3")
        assertThat(BigDecimal(6.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
        interactor.parse("-2   * 3     ")
        assertThat(BigDecimal(-6.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
    }

    @Test
    fun testMultiplicationSuccess() {
        interactor.parse("2.2 * 3.2")
        assertThat(BigDecimal(7.04, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
        interactor.parse("-2.2 * 3.2")
        assertThat(BigDecimal(-7.04, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
        interactor.parse("-2.2 * -3.2")
        assertThat(BigDecimal(7.04, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))

        interactor.parse("2")
        assertThat(BigDecimal(2.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))

        interactor.parse("1*2*3*4*5*6*7*8*9*10")
        assertThat(BigDecimal(3628800.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
    }

    @Test
    fun testDivisionSuccess() {
        interactor.parse("2 / 2")
        assertThat(BigDecimal(1.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))

        interactor.parse("2 / -0.5")
        assertThat(BigDecimal(-4.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))

        interactor.parse("1 / 3")
        assertThat(BigDecimal(1).divide(BigDecimal(3), PRECISION, BigDecimal.ROUND_HALF_UP), Matchers.comparesEqualTo(interactor.product()))

        interactor.parse("3628800/1/2/3/4/5/6/7/8/9/10")
        assertThat(BigDecimal(1.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.product()))
    }

    @Test
    fun testMultiplicationFail() {
        interactor.parse(" * ")
        try {
            interactor.product()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }

        interactor.parse(" 2 * ")
        try {
            interactor.product()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }

        interactor.parse(" 2 * x")
        try {
            interactor.product()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }

        interactor.parse("*2*2")
        try {
            interactor.product()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }
    }

    @Test
    fun testSumSuccess() {
        interactor.parse("2.2 + 3.2")
        assertThat(BigDecimal(5.4, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
        interactor.parse("-2.2 + 3.2")
        assertThat(BigDecimal(1.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
        interactor.parse("-2.2+-3.2")
        assertThat(BigDecimal(-5.4, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))

        interactor.parse("2")
        assertThat(BigDecimal(2.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))

        interactor.parse("1+2+3+4+5+6+7+8+9+10")
        assertThat(BigDecimal(55.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
    }

    @Test
    fun testSumFail() {
        interactor.parse(" + ")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }

        interactor.parse(" 2 + ")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }

        interactor.parse(" 2 + x")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }

        interactor.parse("+2+2")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("0", exception.message)
        }
    }

    @Test
    fun testSumWithMultiplicationSuccess() {
        interactor.parse("2+2*2")
        assertThat(BigDecimal(6.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
        interactor.parse("2+2*2+3*3--2/2")
        assertThat(BigDecimal(16.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
    }

    @Test
    fun testSumWithMultiplicationFail() {
        interactor.parse("2+2.*2")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("2", exception.message)
        }
    }

    @Test
    fun testBracketsSuccess() {
        interactor.parse("(2)")
        assertThat(BigDecimal(2.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
        interactor.parse("2*(-3)")
        assertThat(BigDecimal(-6.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
        interactor.parse("-(2*3)")
        assertThat(BigDecimal(-6.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
        interactor.parse("(2+2)*2")
        assertThat(BigDecimal(8.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
        interactor.parse("2+2*(2+3)*(3--2)/2")
        assertThat(BigDecimal(27.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))

        interactor.parse("  3 +    (   2 + 2  )   * 2   ")
        assertThat(BigDecimal(11.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
        interactor.parse(" ( 2 + 1 ) * ( 3 + 2 ) ")
        assertThat(BigDecimal(15.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))

        interactor.parse("3*((((2+2)*2)))")
        assertThat(BigDecimal(24.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))
    }

    @Test
    fun testBracketsFail() {
        interactor.parse("(2+2*2")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("4", exception.message)
        }

        interactor.parse("2+2)*2")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("6", exception.message)
        }

        interactor.parse("(3*(2+2)))*2")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("6", exception.message)
        }

        interactor.parse("3*((((2+2)*2))")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("4", exception.message)
        }
    }

    @Test
    fun testUnexpectedSymbol() {
        interactor.parse("2*2   ")
        assertThat(BigDecimal(4.0, MathContext.DECIMAL64), Matchers.comparesEqualTo(interactor.sum()))

        interactor.parse(" 2 * 2x")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("5", exception.message)
        }

        interactor.parse(" 2 * 2x * 2")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("5", exception.message)
        }

        interactor.parse(" 2 * 2 x * 2")
        try {
            interactor.sum()
            fail("Expected exception")
        } catch (exception: Exception) {
            assertEquals("5", exception.message)
        }
    }

}