package ru.solovgeo.calculator.presentation

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.solovgeo.calculator.R
import ru.solovgeo.calculator.business.ParseInteractor
import ru.solovgeo.calculator.data.ResourceProvider

class MainActivity : AppCompatActivity(), MainView {

    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenter(ParseInteractor(ResourceProvider(this)), this)
        calculate_button.setOnClickListener({
            presenter.calculate(calculator_input_edittext.text.toString())
        })
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun showError(message: String?) {
        val builder = AlertDialog
                .Builder(this)
                .setTitle(R.string.error)
                .setPositiveButton(R.string.ok, null)
        if (message == null) {
            builder.setMessage(R.string.unexpected_error)
        } else {
            builder.setMessage(message)
        }

        builder.create().show()
    }

    override fun showResult(result: String) {
        result_textview.text = result
    }
}
