package ru.solovgeo.calculator.presentation

interface MainView {
    fun showError(message: String?)
    fun showResult(result: String)
}