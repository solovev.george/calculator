package ru.solovgeo.calculator.presentation

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.solovgeo.calculator.business.ParseInteractor
import java.math.BigDecimal
import java.text.DecimalFormat

class MainPresenter(private val interactor: ParseInteractor, private val view: MainView) {

    var disposable: Disposable? = null
    fun calculate(expression: String) {

        disposable?.dispose()
        disposable = interactor.parse(expression)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.showResult(it.format(12))
                }, {
                    view.showError(it.message)
                    it.printStackTrace()
                })
    }

    fun onDestroy() {
        disposable?.dispose()
    }

    private fun BigDecimal.format(fracDigits: Int): String {
        val df = DecimalFormat()
        df.maximumFractionDigits = fracDigits
        return df.format(this)
    }
}
