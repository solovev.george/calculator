package ru.solovgeo.calculator.data

import android.content.Context
import android.support.annotation.StringRes

class ResourceProvider(private val context: Context) : IResourceProvider {

    override fun getString(@StringRes stringId: Int, vararg formatArgs: Any): String =
            context.getString(stringId, *formatArgs)

}