package ru.solovgeo.calculator.data

import android.support.annotation.StringRes

interface IResourceProvider {
    fun getString(@StringRes stringId: Int, vararg formatArgs: Any): String
}