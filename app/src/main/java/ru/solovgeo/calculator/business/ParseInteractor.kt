package ru.solovgeo.calculator.business

import io.reactivex.Single
import ru.solovgeo.calculator.R
import ru.solovgeo.calculator.data.IResourceProvider
import java.math.BigDecimal

/**
 * Grammar for this task:
 *
 * E -> M | M+E
 * M -> T | T*M
 * T -> a | (E)
 *
 * ----------------------------------
 * after LL-grammar transformation:
 *
 * E  -> ME'
 * E' -> ε | +ME'
 * M  -> TM'
 * M' -> ε | *TM'
 * T -> a | (E)
 *
 */
const val EOF = '\u001a'
const val PRECISION = 24

class ParseInteractor(private val resourceProvider: IResourceProvider) {

    private var currentPosition = 0
    private var expression: String = ""
    private var bracketsCount = 0

    fun parse(expression: String): Single<BigDecimal> {
        currentPosition = 0
        bracketsCount = 0
        this.expression = expression

        return Single.create<BigDecimal> {
            try {
                val result = sum()
                it.onSuccess(result)
            } catch (exception: Exception) {
                it.onError(exception)
            }
        }

    }

    fun sum(): BigDecimal {
        var value = product()
        while (true) {
            val token = readTokenSkipWhiteSpace()
            if (token == Nonterminals.PLUS.char) {
                goToNextPosition()
                value += product()
            } else if (token == Nonterminals.MINUS.char) {
                goToNextPosition()
                value -= product()
            } else if (token == EOF) {
                break
            } else if (token == Nonterminals.RP.char) {
                foundRightBracket()
                break
            } else {
                throw Exception(resourceProvider.getString(R.string.exception_unexpected_symbol, currentPosition))
            }
        }
        return value
    }

    fun product(): BigDecimal {
        var value = primitive()
        while (true) {
            val token = readTokenSkipWhiteSpace()
            if (token == Nonterminals.MUL.char) {
                goToNextPosition()
                value *= primitive()
            } else if (token == Nonterminals.DIV.char) {
                goToNextPosition()
                value = value.divide(primitive(), PRECISION, BigDecimal.ROUND_HALF_UP)
            } else {
                break
            }
        }
        return value
    }

    fun primitive(): BigDecimal {
        val token = readTokenSkipWhiteSpace()
        return when {
            token.isDigit() -> getNumber()
            token == Nonterminals.MINUS.char -> {
                goToNextPosition()
                primitive() * BigDecimal(-1)
            }
            token == Nonterminals.LP.char -> {
                foundLeftBracket()
                goToNextPosition()
                val sum = sum()
                if (readTokenSkipWhiteSpace() != Nonterminals.RP.char) {
                    throw Exception(resourceProvider.getString(R.string.exception_expect_right_bracket, currentPosition))
                } else {
                    goToNextPosition()
                    return sum
                }
            }
            else -> throw Exception(resourceProvider.getString(R.string.exception_expect_number, currentPosition))
        }
    }

    fun getNumber(): BigDecimal {
        if (currentPosition >= expression.length) {
            throw Exception(resourceProvider.getString(R.string.exception_expect_number, currentPosition))
        }

        readTokenSkipWhiteSpace()
        val numberStartIndex = currentPosition
        var hasDot = false

        while (true) {
            if (currentPosition >= expression.length) {
                break
            }

            var token = expression[currentPosition]

            if (token == '.') {
                if (!hasDot) {
                    hasDot = true

                    goToNextPosition()
                    if (currentPosition >= expression.length) {
                        throw Exception(resourceProvider.getString(R.string.exception_nothing_after_dot, currentPosition))
                    }

                    token = expression[currentPosition]
                    if (!token.isDigit()) {
                        if (token.isWhitespace()) {
                            throw Exception(resourceProvider.getString(R.string.exception_nothing_after_dot, currentPosition))
                        }
                        throw Exception(resourceProvider.getString(R.string.exception_wrong_symbol_in_number, currentPosition))
                    }

                } else {
                    throw Exception(resourceProvider.getString(R.string.exception_two_dots, currentPosition))
                }
            }

            if (!token.isDigit()) {
                break
            }

            goToNextPosition()
        }

        if (currentPosition - numberStartIndex == 0) {
            throw Exception(resourceProvider.getString(R.string.exception_expect_number, currentPosition))
        }

        return expression.substring(numberStartIndex, currentPosition).toBigDecimal()
    }

    private fun readTokenSkipWhiteSpace(): Char {
        if (currentPosition >= expression.length) {
            return EOF
        }

        while (expression[currentPosition].isWhitespace()) {
            goToNextPosition()
            if (currentPosition >= expression.length) {
                return EOF
            }
        }

        return expression[currentPosition]
    }

    private fun goToNextPosition() {
        ++currentPosition
    }

    private fun foundLeftBracket() {
        bracketsCount++
    }

    private fun foundRightBracket() {
        bracketsCount--
        if (bracketsCount < 0) {
            throw Exception(resourceProvider.getString(R.string.exception_unexpected_right_bracket, currentPosition))
        }
    }
}