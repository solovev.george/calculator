package ru.solovgeo.calculator.business

enum class Nonterminals(val char: Char) {
    PLUS('+'),
    MINUS('-'),
    MUL('*'),
    DIV('/'),
    LP('('),
    RP(')')
}